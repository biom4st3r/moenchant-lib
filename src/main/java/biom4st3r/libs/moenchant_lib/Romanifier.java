package biom4st3r.libs.moenchant_lib;

import java.util.TreeMap;

import net.minecraft.util.Util;

import com.google.common.collect.Maps;
import org.jetbrains.annotations.ApiStatus.AvailableSince;

@AvailableSince("0.2.0")
public final class Romanifier {

    @AvailableSince("0.2.0")
    public static String toRoman(int num) {
        // StackOverflow
        int l = romanNumeralMap.floorKey(num);
        if (num == l) {
            return romanNumeralMap.get(num);
        }
        return romanNumeralMap.get(l) + toRoman(num - l);
    }
    
    private final static TreeMap<Integer, String> romanNumeralMap = Util.make(Maps.newTreeMap(), map -> {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");
    });
}
