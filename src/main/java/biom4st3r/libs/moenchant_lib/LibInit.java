package biom4st3r.libs.moenchant_lib;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Iterator;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import biom4st3r.libs.moenchant_lib.builder.EnchantBuilder;
import biom4st3r.libs.moenchant_lib.config_test.EnchantmentOverride;
import biom4st3r.libs.moenchant_lib.config_test.EnchantmentOverride.JsonHandler;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.util.registry.Registry;

public class LibInit {
    public static final BioLogger logger = new BioLogger("Mo'Enchant-Lib");
    public static final Supplier<Class<?>> CLAZZ = Suppliers.memoize(()-> EnchantBuilder.newBuilder(Rarity.COMMON).build("dummy").getClass());

    public static JsonHandler enchantmentOverrides; 
    static Gson gson = new GsonBuilder().registerTypeAdapter(EnchantmentOverride.class, new EnchantmentOverride()).create();
    public static boolean enableBetterAnvil;

    @AvailableSince("0.1.1")
    public static ConvientOptional<EnchantmentOverride> getOveride(String s) {
        enchantmentOverrides.reset();
        if (!enchantmentOverrides.isInvalid() && LibInit.enchantmentOverrides.get(s).isPresent()) {
            return ConvientOptional.of(gson.fromJson(enchantmentOverrides.getCurrentElement(), EnchantmentOverride.class));
        }
        return ConvientOptional.empty();
    }

    public static Iterator<Enchantment> getVanillaStyleEnchantments() {
        return Registry.ENCHANTMENT.stream().filter(e -> !ExtendedEnchantment.cast(e).isExtended()).iterator();
    }

    static {
        try {
            File file = new File(FabricLoader.getInstance().getConfigDir().toFile(), "moenchantment_overrides.json");
            if (!file.exists()) file.createNewFile();
            byte[] b = Files.readAllBytes(file.toPath());
            Gson builder = new GsonBuilder().create();
            enchantmentOverrides = JsonHandler.of(builder.fromJson(new String(b), JsonObject.class));
        } catch (IOException e) {
            logger.log("moenchantment_overrides.json not found");
        }
    }
}
