package biom4st3r.libs.moenchant_lib.builder;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.ApiStatus.ScheduledForRemoval;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.LibInit;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.AnvilAcceptibleContext;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.ModifyDamageFunction;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment.TriConsumer;
import biom4st3r.libs.moenchant_lib.config_test.EnchantmentOverride;
import biom4st3r.libs.moenchant_lib.events.BlockBreakEvent;
import biom4st3r.libs.moenchant_lib.events.OnBowArrowCreationEvent;
import biom4st3r.libs.moenchant_lib.events.OnCrossBowArrowCreationEvent;
import biom4st3r.net.objecthunter.exp4j.Expression;
import it.unimi.dsi.fastutil.ints.Int2IntFunction;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;

/**
 * MoEnchantBuilder
 * Enchantment Builder
 */
@AvailableSince("0.1.0")
public class EnchantBuilder {
    EnchantmentSkeleton DELEGATE;

    public enum ApplicationSource {
        ENCHANTMENT_TABLE,
        RANDOM_LOOT_FUNC,
        ANVIL,
    }

    @AvailableSince("0.2.0")
    public static EnchantBuilder newBuilder(Rarity rarity, EquipmentSlot... slots) {
        Preconditions.checkNotNull(rarity);
        Preconditions.checkNotNull(slots);
        if (slots.length == 0) LibInit.logger.error("your EnchantBuilder didn't supply any slots. This is probably an accident.");

        return new EnchantBuilder(rarity, slots);
    }

    @AvailableSince("0.1.0")
    public static interface GenericArrowCreativeEvent {
        void onCreation(PersistentProjectileEntity arrow, ItemStack crossBow, ItemStack arrowItem, LivingEntity shooter);
    }

    @AvailableSince("0.1.0")
    public static interface PseudoGetPossibleEnchantments {
        void apply(Enchantment enchantment, int power, ItemStack stack, boolean treasureAllowed, List<EnchantmentLevelEntry> list);
    }

    /**
     * Called whenever a player breaks a block. You must explicitly check for your enchantment
     * @param stage
     * @param event
     * @return
     */
    @AvailableSince("0.4.2")
    @Deprecated(forRemoval = true)
    @ScheduledForRemoval(inVersion = "0.4.5")
    public EnchantBuilder onBlockBreak(BlockBreakEvent.BreakStage stage, BlockBreakEvent.OnBreakEvent event) {
        Preconditions.checkNotNull(stage);
        Preconditions.checkNotNull(event);

        BlockBreakEvent.register(stage, event);

        return this;
    }

    public static interface TriPredicate<A,B,C> {
        boolean test(A a, B b, C c);
    }

    @AvailableSince("0.4.2")
    public EnchantBuilder setName(IntFunction<Text> text) {
        ObjKeys.GET_NAME.set(this.DELEGATE, text);
        return this;
    }

    public static interface BlockBreakContext {
        boolean onBreak(ServerPlayerEntity player, ItemStack tool, int level, BlockPos pos);
    }

    @AvailableSince("tbd")
    public EnchantBuilder preBlockBreak(BlockBreakContext predicate) {
        ObjKeys.PRE_BLOCK_BREAK.set(this.DELEGATE, predicate);
        return this;
    }

    @AvailableSince("tbd")
    public EnchantBuilder postBlockBreak(BlockBreakContext predicate) {
        ObjKeys.POST_BLOCK_BREAK.set(this.DELEGATE, predicate);
        return this;
    }

    @AvailableSince("0.2.0")
    public EnchantBuilder addAttributeModifiers(EquipmentSlot slot, Consumer<Multimap<EntityAttribute, EntityAttributeModifier>> consumer) {
        Preconditions.checkNotNull(slot);
        Preconditions.checkNotNull(consumer);

        Multimap<EntityAttribute, EntityAttributeModifier> multiMap = this.DELEGATE.get(ObjKeys.ENTITY_ATTRIBUTES).computeIfAbsent(slot, s->HashMultimap.create());
        consumer.accept(multiMap);

        return this;
    }

    @AvailableSince("0.1.0")
    public EnchantBuilder(Rarity rarity, EquipmentSlot... slots) {
        Preconditions.checkNotNull(rarity, "MoenchantmentBuilder ctor(rarity) does not accept null" );
        Preconditions.checkNotNull(slots, "MoenchantmentBuilder ctor(slots) does not accept null");
        this.DELEGATE = new EnchantmentSkeleton(rarity, slots);
    }

    /**
     * For {@link EnchantBuilder#modEnchantment(EnchantmentSkeleton, Consumer)}
     * @param skeleton
     */
    private EnchantBuilder(EnchantmentSkeleton skeleton) {
        this.DELEGATE = skeleton;
    }

    /**
     * When this enchantment is added to an itemstack.
     * @param consumer
     * @return
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder whenAddedToStack(Consumer<ItemStack> consumer) {
        Preconditions.checkNotNull(consumer, "MoenchantmentBuilder.whenAddedToStack() does not accept null");
        ObjKeys.WHEN_ADDED_TO_STACK.set(this.DELEGATE, consumer);
        return this;
    }

    /**
     * If this enchantment can be applied from the enchantment table or enchantment loot functions
     * @param bool
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder fromEnchantmentTableAndLoot(boolean bool) {
        Booleans.IS_AVAILABLE_FOR_RANDOM_SELECTION.set(this.DELEGATE, bool);
        return this;
    }

    /**
     * If villagers and offer this enchantment
     * @param bool
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder fromLibrarian(boolean bool) {
        Booleans.IS_AVAILABLE_FOR_ENCHANTED_BOOK_OFFER.set(this.DELEGATE, bool);
        return this;
    }

    /**
     * Wheather or not this enchantments should be registered
     * @param bool
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder enabled(boolean bool) {
        Booleans.ENABLED.set(this.DELEGATE, bool);
        return this;
    }

    /**
     * Delcares this Enchantment as a treasure
     * @param bool
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder treasure(boolean bool) {
        Booleans.IS_TREASURE.set(this.DELEGATE, bool);
        return this;
    }

    /**
     * Delcares this Enchantment as a curse
     * @param bool
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder curse(boolean bool) {
        Booleans.IS_CURSE.set(this.DELEGATE, bool);
        return this;
    }

    /**
     * Sets {@code ExtendedEnchantment#providesApplicationLogic()} to true and <p>
     * provides the alternative logic to be used and {@link net.minecraft.enchantment.EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}
     * @param func
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder setApplicationLogic(PseudoGetPossibleEnchantments func) {
        Preconditions.checkNotNull(func, "MoenchantmentBuilder.setApplicationLogic() does not accept null");
        Booleans.HAS_APPLY_LOGIC.set(this.DELEGATE, true);
        ObjKeys.APPLY_LOGIC.set(this.DELEGATE, func);
        return this;
    }

    /**
     * 
     * @param minLevel
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder minlevel(int minLevel) {
        Preconditions.checkArgument(minLevel > 0, "MoenchantmentBuilder.minlevel() does not accept Less than 0");
        Ints.MIN_LEVEL.set(this.DELEGATE, minLevel);
        return this;
    }

    /**
     * 
     * @param maxLevel
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder maxlevel(int maxLevel) {
        Preconditions.checkArgument(maxLevel > 0, "MoenchantmentBuilder.maxlevel() does not accept Less than 0");
        Ints.MAX_LEVEL.set(this.DELEGATE, maxLevel);
        return this;
    }

    /**
     * 
     * @param provider
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder minpower(Int2IntFunction provider) {
        Preconditions.checkNotNull(provider, "MoenchantmentBuilder.minpower() does not accept null");
        ObjKeys.MIN_POWER.set(this.DELEGATE, provider);
        return this;
    }

    /**
     * 
     * @param provider
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder maxpower(Int2IntFunction provider) {
        Preconditions.checkNotNull(provider, "MoenchantmentBuilder.maxpower() does not accept null");
        ObjKeys.MAX_POWER.set(this.DELEGATE, provider);
        return this;
    }

    /**
     * Specifies enchantments that should be on the same item as this.
     * @param e
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder addExclusive(Enchantment e) {
        Preconditions.checkNotNull(e, "MoenchantmentBuilder.addExclusive() does not accept null");
        ObjKeys.EXCLUSIVE_ENCHANTMENTS.get(this.DELEGATE).add(e);
        return this;
    }

    @AvailableSince("0.2.0")
    public EnchantBuilder addExclusive(ExtendedEnchantment e) {
        Preconditions.checkNotNull(e, "MoenchantmentBuilder.addExclusive() does not accept null");
        ObjKeys.EXCLUSIVE_ENCHANTMENTS.get(this.DELEGATE).add(e.asEnchantment());
        return this;
    }

    /**
     * Specifies enchantments that should be on the same item as this.
     * @param e
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder addExclusive(Enchantment... e) {
        Preconditions.checkNotNull(e, "MoenchantmentBuilder.addExclusive() does not accept null");
        Collections.addAll(ObjKeys.EXCLUSIVE_ENCHANTMENTS.get(this.DELEGATE), e);

        return this;
    }

    /**
     * Provides extra logic for when a bow with this enchantment creates an arrow entity.
     * @param event
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder onArrowCreatedFromBow(OnBowArrowCreationEvent event) {
        Preconditions.checkNotNull(event, "MoenchantmentBuilder.onArrowCreatedFromBow() does not accept null");
        ObjKeys.ON_BOW_ARROW_CREATED.set(this.DELEGATE, event);
        return this;
    }

    /**
     * Provides extra logic for when a bow or crossbow with this enchantment creates an arrow entity.
     * @param event
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder onArrowCreated(GenericArrowCreativeEvent event) {
        Preconditions.checkNotNull(event, "MoenchantmentBuilder.onArrowCreated() does not accept null");

        this.onArrowCreatedFromCrossbow((arrow, crossBow, arrowItem, shooter) -> {
            event.onCreation(arrow, crossBow, arrowItem, shooter);
        });

        this.onArrowCreatedFromBow((bow, arrowStack, arrowEntity, player, elapsed, pullProg) -> {
            event.onCreation(arrowEntity, bow, arrowStack, player);
        });
        return this;
    }

    /**
     * Provides extra logic for when a crossbow with this enchantment creates an arrow entity.
     * @param event
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder onArrowCreatedFromCrossbow(OnCrossBowArrowCreationEvent event) {
        Preconditions.checkNotNull(event, "MoenchantmentBuilder.onArrowCreatedFromCrossbow() does not accept null");
        ObjKeys.ON_CROSSBOW_ARROW_CREATED.set(this.DELEGATE, event);
        return this;
    }

    /**
     * Adds the enchantment to arrows created from bows with this enchantment. Similar to what Vanilla does with punch<p>
     * to check the enchantments a Projectile entity has use EnchantableProjectileEntity duck.
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder applyEnchantmentToArrowsFromBows() {
        this.init_data_tracker_for_arrows();
        Booleans.TRANSFERS_FROM_BOW_TO_ARROW.set(this.DELEGATE, true);
        return this;
    }

    /**
     * Addeds the enchantment to arrows created from bows or crossbows with this enchantment. Similar to what Vanilla does with punch<p>
     * to check an Projectile entity with an enchantments use EnchantableProjectileEntity
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder applyEnchantmentToArrows() {
        this.init_data_tracker_for_arrows();
        this.applyEnchantmentToArrowsFromBows();
        this.applyEnchantmentToArrowsFromCrossbows();
        return this;
    }

    @Internal
    private void init_data_tracker_for_arrows() {
        if (DELEGATE.get(ObjKeys.PROJECTILE_TRACKED_DATA) != null) return;
        TrackedData<Byte> i = DataTracker.registerData(PersistentProjectileEntity.class, TrackedDataHandlerRegistry.BYTE);
        ObjKeys.PROJECTILE_TRACKED_DATA.set(this.DELEGATE, i);
        ExtendedEnchantment.TRACKED_ARROW_DATA_KEYS.put(this.DELEGATE, i);
    }

    /**
     * Addeds the enchantment to arrows created from crossbows with this enchantment. Similar to what Vanilla does with punch<p>
     * to check an Projectile entity with an enchantments use EnchantableProjectileEntity
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder applyEnchantmentToArrowsFromCrossbows() {
        this.init_data_tracker_for_arrows();

        Booleans.TRANSFERS_FROM_CROSSBOW_TO_ARROW.set(this.DELEGATE, true);
        return this;
    }

    /**
     * Used for everything unless otherwise specified within this Builder.
     * @param isAcceptible
     * @return this
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder isAcceptible(Predicate<ItemStack> isAcceptible) {
        Preconditions.checkNotNull(isAcceptible, "MoenchantmentBuilder.isAcceptible() does not accept null");
        ObjKeys.IS_ACCEPTIBLE.set(this.DELEGATE, isAcceptible);
        return this;
    }

    @AvailableSince("0.4.2")
    public EnchantBuilder isAcceptibleInAnvil(AnvilAcceptibleContext ctx) {
        Preconditions.checkNotNull(ctx, "MoenchantmentBuilder.isAcceptibleInAnvil(AnvilAcceptibleContext) does not accept null");
        ObjKeys.IS_ACCEPTIBLE_IN_ANVIL_2.set(this.DELEGATE, ctx);
        return this;
    }

    @AvailableSince("0.1.0")
    @Deprecated(forRemoval = true)
    @ScheduledForRemoval(inVersion = "0.4.5")
    public EnchantBuilder isAcceptibleInAnvil(BiFunction<ItemStack, AnvilScreenHandler, Boolean> isAcceptible) {
        Preconditions.checkNotNull(isAcceptible, "MoenchantmentBuilder.isAcceptibleInAnvil() does not accept null");
        ObjKeys.IS_ACCEPTIBLE_IN_ANVIL.set(this.DELEGATE, isAcceptible);
        return this;
    }

    @AvailableSince("0.1.0")
    public EnchantBuilder isAcceptibleOnBook(boolean b) {
        Booleans.IS_ACCEPTIBLE_ON_BOOK.set(this.DELEGATE, b);
        return this;
    }

    @AvailableSince("0.1.0")
    public EnchantBuilder isAcceptibleFromRandomLootFunc(BiFunction<ItemStack, LootContext, Boolean> func) {
        Preconditions.checkNotNull(func);
        ObjKeys.IS_ACCEPTABLE_FOR_RANDOM_LOOT_FUNC.set(this.DELEGATE, func);
        return this;
    }

    @AvailableSince("0.1.0")
    /**
     * Runs after modifyDamageOutput. If a LivingEntity is hit while wearing/holding and Item with this enchantment this will be called.<p>
     * FAIL - cancels all damage and stops further execution<p>
     * PASS - does nothing and continues to the next<p>
     * CONSUME - sets the damage to the provided amount and continues to the next<p>
     * SUCCESS - undefined<p>
     * @param func
     * @return
     */
    public EnchantBuilder modifyIncomingDamage(ModifyDamageFunction func) {
        Preconditions.checkNotNull(func);
        ObjKeys.MODIFY_INCOMING_DAMAGE.set(this.DELEGATE, func);
        return this;
    }
    /**
     * Runs before modifyIncomingDamage. If a LivingEntity is hit with an Item with this enchantment this will be called.<p>
     * FAIL - cancels all damage and stops further execution<p>
     * PASS - does nothing and continues to the next<p>
     * CONSUME - sets the damage to the provided amount and continues to the next<p>
     * SUCCESS - undefined<p>
     * @param func
     * @return
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder modifyDamageOutput(ModifyDamageFunction func) {
        Preconditions.checkNotNull(func);
        ObjKeys.MODIFY_DAMAGE_OUTPUT.set(this.DELEGATE, func);
        return this;
    }

    /**
     * 
     * @param func [user, attacker, level]
     * @return
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder onTargetDamaged(TriConsumer<LivingEntity, Entity, Integer> func) {
        Preconditions.checkNotNull(func);
        ObjKeys.ON_TARGET_DAMAGED.set(this.DELEGATE, func);
        return this;
    }
    /**
     * 
     * @param func user, attacker, level
     * @return
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder onUserDamaged(TriConsumer<LivingEntity, Entity, Integer> func) {
        Preconditions.checkNotNull(func);
        ObjKeys.ON_USER_DAMAGED.set(this.DELEGATE, func);
        return this;
    }

    /**
     * 
     * @param func level, DamageSource -> amount
     * @return
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder protectionAmount(BiFunction<Integer, DamageSource, Integer> func) {
        Preconditions.checkNotNull(func);
        ObjKeys.GET_PROTECTION_AMOUNT.set(this.DELEGATE, func);
        return this;
    }

    /**
     * 
     * @param func level, group -> amount
     * @return
     */
    @AvailableSince("0.1.0")
    public EnchantBuilder attackDamage(BiFunction<Integer, EntityGroup, Float> func) {
        Preconditions.checkNotNull(func);
        ObjKeys.GET_ATTACK_DAMAGE.set(this.DELEGATE, func);
        return this;
    }

    static Gson gson = new GsonBuilder().registerTypeAdapter(EnchantmentOverride.class, new EnchantmentOverride()).create();

    @AvailableSince("0.1.0")
    public ExtendedEnchantment build(String registerName) {
        Preconditions.checkNotNull(registerName, "MoenchantmentBuilder.build() does not accept null");
        Preconditions.checkArgument(!registerName.isEmpty(), "MoenchantmentBuilder.build() does not accept empty regnames");
        
        LibInit.enchantmentOverrides.reset();
        if (LibInit.getOveride(registerName).getOrNull() instanceof EnchantmentOverride override) {
            if (override.curse != null) {
                this.curse(override.curse);
            }
            if (override.enabled != null) {
                this.enabled(override.enabled);
            }
            if (override.treasure != null) {
                this.treasure(override.treasure);
            }
            if (override.max_level != null) {
                this.maxlevel(override.max_level);
            }
            if (override.min_level != null) {
                this.minlevel(override.min_level);
            }
            if (override.min_power != null) {
                Expression ex = override.min_power;
                this.minpower(i  ->  (int)ex.setVariable("level", i).evaluate());
            }
            if (override.max_power != null) {
                Expression ex = override.max_power;
                this.maxpower(i  ->  (int)ex.setVariable("level", i).evaluate());
            }
        }
        ObjKeys.REG_NAME.set(this.DELEGATE, registerName.toLowerCase());
        if (this.DELEGATE.enabled()) {
            ENCHANTMENTS.add(this.DELEGATE);
        }
        return this.DELEGATE;
    }

    @Internal
    public ExtendedEnchantment buildAndRegister(String registerName) {
        ExtendedEnchantment e = this.build(registerName);
        if(this.DELEGATE.isEnabled()) {
            Registry.register(Registry.ENCHANTMENT, new Identifier("moenchantments", registerName), e.asEnchantment());
        }
        return e;
    }

    @AvailableSince("0.2.0")
    public ExtendedEnchantment buildAndRegister(Identifier id) {
        Preconditions.checkNotNull(id);
        ExtendedEnchantment e = this.build(id.toString());
        Registry.register(Registry.ENCHANTMENT, id, e.asEnchantment());
        return e;
    }

    private static final Set<ExtendedEnchantment> ENCHANTMENTS = Sets.newHashSet();
    private static ExtendedEnchantment[] INTERNAL_ENCHANTMENTS = null;
    
    public static final Stream<ExtendedEnchantment> getEnchantments() {
        if (INTERNAL_ENCHANTMENTS == null) INTERNAL_ENCHANTMENTS = ENCHANTMENTS.toArray(new EnchantmentSkeleton[0]);
        return Stream.of(INTERNAL_ENCHANTMENTS);
    }

    /**
     * You probably don't need this.
     * @param enchant
     * @param consumer
     */
    @Deprecated(forRemoval = false)
    @Internal
    public static void modEnchantment(ExtendedEnchantment enchant, Consumer<EnchantBuilder> consumer) {
        if(enchant instanceof EnchantmentSkeleton skele) {
            if (Registry.ENCHANTMENT.getRawId(enchant.asEnchantment()) != -1) throw new IllegalStateException("Enchantments can't be modified after being registered.");
            consumer.accept(new EnchantBuilder(skele));
        }
    }
}