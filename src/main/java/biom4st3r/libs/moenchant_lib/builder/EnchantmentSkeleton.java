package biom4st3r.libs.moenchant_lib.builder;

import java.util.EnumMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import org.jetbrains.annotations.ApiStatus.Internal;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.LibInit;
import biom4st3r.libs.moenchant_lib.Romanifier;
import biom4st3r.libs.moenchant_lib.builder.ObjKeys.Key;
import it.unimi.dsi.fastutil.ints.Int2IntFunction;

import net.fabricmc.loader.api.FabricLoader;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;

/**
 * Buildable enchantment implementation
 */
@Internal
final class EnchantmentSkeleton extends Enchantment implements ExtendedEnchantment {

    // https://github.com/svenhjol/Charm/blob/2c73cf726dd5d33bf01b7241418304666465de21/src/main/java/svenhjol/charm/mixin/no_treasure_enchantment_trading/CheckIfTradeableMixin.java#L17
    public static final boolean isCharmLoaded = FabricLoader.getInstance().isModLoaded("charm");

    public static Optional<EnchantmentSkeleton> isSkeleton(Enchantment e) {
        return (e instanceof EnchantmentSkeleton es) ? Optional.of(es) : Optional.empty();
    }

    final boolean[] BOOLEANS = Util.make(new boolean[Booleans.values().length], array -> {
        array[Booleans.ENABLED.ordinal()] = false;
        array[Booleans.IS_TREASURE.ordinal()] = super.isTreasure();
        array[Booleans.IS_CURSE.ordinal()] = super.isCursed();
        array[Booleans.TRANSFERS_FROM_BOW_TO_ARROW.ordinal()] = false;
        array[Booleans.TRANSFERS_FROM_CROSSBOW_TO_ARROW.ordinal()] = false;
        array[Booleans.IS_AVAILABLE_FOR_ENCHANTED_BOOK_OFFER.ordinal()] = isCharmLoaded ? true : super.isAvailableForEnchantedBookOffer();
        array[Booleans.IS_AVAILABLE_FOR_RANDOM_SELECTION.ordinal()] = super.isAvailableForRandomSelection();
        array[Booleans.HAS_APPLY_LOGIC.ordinal()] = false;
        array[Booleans.IS_ACCEPTIBLE_ON_BOOK.ordinal()] = true;
    });

    final int[] INTS = Util.make(new int[Ints.values().length], array -> {
        array[Ints.MIN_LEVEL.ordinal()] = super.getMinLevel();
        array[Ints.MAX_LEVEL.ordinal()] = super.getMaxLevel();
    });

    final Object[] OBJS = new Object[ObjKeys.size()]; { //
        ObjKeys.REG_NAME.set(this, "");
        ObjKeys.IS_ACCEPTIBLE.set(this, stack -> {
            LibInit.logger.debug("Please replace isAcceptible for %s", ObjKeys.REG_NAME.get(this));
            return false;
        });
        ObjKeys.IS_ACCEPTIBLE_IN_ANVIL.set(this, (is, as) -> this.isAcceptable(is));
        ObjKeys.APPLY_LOGIC.set(this, (a, b, c, d, e) -> {});
        ObjKeys.MIN_POWER.set(this, super::getMinPower);
        ObjKeys.MAX_POWER.set(this, super::getMaxPower);
        ObjKeys.EXCLUSIVE_ENCHANTMENTS.set(this, Lists.newArrayList());
        ObjKeys.IS_ACCEPTABLE_FOR_RANDOM_LOOT_FUNC.set(this, (is, ctx) -> this.isAcceptable(is));
        ObjKeys.WHEN_ADDED_TO_STACK.set(this, stack -> {});
        ObjKeys.PROJECTILE_TRACKED_DATA.set(this, null);
        ObjKeys.ON_TARGET_DAMAGED.set(this, super::onTargetDamaged);
        ObjKeys.ON_USER_DAMAGED.set(this, super::onUserDamaged);
        ObjKeys.GET_PROTECTION_AMOUNT.set(this, super::getProtectionAmount);
        ObjKeys.GET_ATTACK_DAMAGE.set(this, super::getAttackDamage);
        ObjKeys.MODIFY_INCOMING_DAMAGE.set(this, ExtendedEnchantment.super::modifyIncomingDamage);
        ObjKeys.MODIFY_DAMAGE_OUTPUT.set(this, ExtendedEnchantment.super::modifyDamageOutput);
        ObjKeys.ENTITY_ATTRIBUTES.set(this, new EnumMap<>(EquipmentSlot.class));
        ObjKeys.IS_ACCEPTIBLE_IN_ANVIL_2.set(this, (l,r,o,consumer) -> AnvilAction.PASS);
        ObjKeys.PRE_BLOCK_BREAK.set(this, ExtendedEnchantment.super::preBlockBreak);
        ObjKeys.POST_BLOCK_BREAK.set(this, ExtendedEnchantment.super::postBlockBreak);
        ObjKeys.ON_CROSSBOW_ARROW_CREATED.set(this, ExtendedEnchantment.super::onCrossbowArrowCreation);
        ObjKeys.ON_BOW_ARROW_CREATED.set(this, ExtendedEnchantment.super::onBowArrowCreation);
        ObjKeys.GET_NAME.set(this, null);
    }

    @SuppressWarnings({"unchecked"})
    <T> T get(Key<T> key) {
        return (T) OBJS[key.ordinal()];
    }
    boolean get(Booleans bool) {
        return BOOLEANS[bool.ordinal()];
    }
    int get(Ints intt) {
        return INTS[intt.ordinal()];
    }
    <T> void set(Key<T> key, T t) {
        this.OBJS[key.ordinal()] = t;
    }
    void set(Booleans bools, boolean b) {
        BOOLEANS[bools.ordinal()] = b;
    }
    void set(Ints is, int i) {
        INTS[is.ordinal()] = i;
    }

    @Override
    public boolean isEnabled() {
        return this.BOOLEANS[Booleans.ENABLED.ordinal()];
    }

    @Override
    public boolean transfersFromBowToArrow() {
        return this.BOOLEANS[Booleans.TRANSFERS_FROM_BOW_TO_ARROW.ordinal()];
    }
    @Override
    public boolean transfersFromCrossbowToArrow() {
        return this.BOOLEANS[Booleans.TRANSFERS_FROM_CROSSBOW_TO_ARROW.ordinal()];
    }

    @Override
    public Multimap<EntityAttribute, EntityAttributeModifier> getEntityAttributes(EquipmentSlot slot) {

        return ObjKeys.ENTITY_ATTRIBUTES.get(this).getOrDefault(slot, ImmutableMultimap.of());
    }

    @Override
    public TrackedData<Byte> getProjectileEnchantmentTrackedData() {
        return ObjKeys.PROJECTILE_TRACKED_DATA.get(this);
    }

    public final int getLevelFromProjectile(ProjectileEntity entity) {
        if (this.getProjectileEnchantmentTrackedData() == null) return 0;
        if (entity == null) return 0;
        return entity.getDataTracker().get(this.getProjectileEnchantmentTrackedData());
    }

    public final void setLevelFromProjectile(ProjectileEntity entity, int level) {
        if (this.getProjectileEnchantmentTrackedData() == null) return;
        if (entity == null) return;
        entity.getDataTracker().set(getProjectileEnchantmentTrackedData(), (byte)level);
    }

    @Override
    public TypedActionResult<Float> modifyIncomingDamage(int level, LivingEntity target, float damage,
            DamageSource source) {
        return ObjKeys.MODIFY_INCOMING_DAMAGE.get(this).modifyDamage(level, target, damage, source);
        // return this.modifyIncomingDamage.modifyDamage(level, target, damage, source);
    }

    @Override
    public TypedActionResult<Float> modifyDamageOutput(int level, LivingEntity target, float damage,
            DamageSource source) {
        return ObjKeys.MODIFY_DAMAGE_OUTPUT.get(this).modifyDamage(level, target, damage, source);
        // return this.modifyDamageOutput.modifyDamage(level, target, damage, source);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {
        ObjKeys.ON_TARGET_DAMAGED.get(this).accept(user, target, level);
        // this.onTargetDamaged.accept(user, target, level);
    }

    @Override
    public void onUserDamaged(LivingEntity user, Entity attacker, int level) {
        ObjKeys.ON_USER_DAMAGED.get(this).accept(user, attacker, level);
        // this.onUserDamaged.accept(user, attacker, level);
    }

    @Override
    public int getProtectionAmount(int level, DamageSource source) {
        return ObjKeys.GET_PROTECTION_AMOUNT.get(this).apply(level, source);
        // return this.getProtectionAmount.apply(level, source);
    }

    @Override
    public float getAttackDamage(int level, EntityGroup group) {
        return ObjKeys.GET_ATTACK_DAMAGE.get(this).apply(level, group);
        // return this.getAttackDamage.apply(level, group);
    }

    @Override
    public void enchantmentAddedToStack(ItemStack stack) {
        ObjKeys.WHEN_ADDED_TO_STACK.get(this).accept(stack);
        // this.whenAddedToStack.accept(stack);
    }

    @Override
    public boolean providesApplicationLogic() {
        return BOOLEANS[Booleans.HAS_APPLY_LOGIC.ordinal()];
    }

    @Override
    public boolean isAvailableForEnchantedBookOffer() {
        return BOOLEANS[Booleans.IS_AVAILABLE_FOR_ENCHANTED_BOOK_OFFER.ordinal()];
    }

    @Override
    public boolean isAvailableForRandomSelection() {
        return BOOLEANS[Booleans.IS_AVAILABLE_FOR_RANDOM_SELECTION.ordinal()];
    }


    @Override
    public Text moenchant$getName(int lvl) {
        return ObjKeys.GET_NAME.get(this).apply(lvl);
    }

    @Override
    public Text getName(int level) {
        if (ObjKeys.GET_NAME.get(this) != null) return this.moenchant$getName(level);

        MutableText text = Text.translatable(this.getTranslationKey(), new Object[0]);
        if (this.isCursed()) {
            text.formatted(Formatting.RED);
        } else {
            text.formatted(Formatting.GRAY);
        }
        if (level != 1 || this.getMaxLevel() != 1) {
            text.append(" ").append(Text.translatable(Romanifier.toRoman(level)));
        }
        return text;
    }

    public interface LevelProvider extends Int2IntFunction {}

    public EnchantmentSkeleton(Rarity r, EquipmentSlot[] es) {
        super(r, ExtendedEnchantment.target, es);
        ObjKeys.EXCLUSIVE_ENCHANTMENTS.get(this).add(this);
        // this.exclusiveEnchantments.add(this);
    }

    @Override
    public boolean isCursed() {
        return BOOLEANS[Booleans.IS_CURSE.ordinal()];
    }

    @Override
    public boolean isTreasure() {
        return BOOLEANS[Booleans.IS_TREASURE.ordinal()];
    }

    @Internal
    @Override
    public String regName() {
        if (ObjKeys.REG_NAME.get(this).isEmpty()) throw new IllegalStateException("EnchantmentSkeleton#regName called before enchantment was built");
        return ObjKeys.REG_NAME.get(this);
    }

    public boolean enabled() {
        return BOOLEANS[Booleans.ENABLED.ordinal()];
    }

    @Override
    public int getMaxLevel() {
        return INTS[Ints.MAX_LEVEL.ordinal()];
    }

    @Override
    public int getMinLevel() {
        return INTS[Ints.MIN_LEVEL.ordinal()];
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        return ObjKeys.IS_ACCEPTIBLE.get(this).test(iS);
        // return isAcceptible.test(iS);
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !ObjKeys.EXCLUSIVE_ENCHANTMENTS.get(this).contains(other);
        // return !exclusiveEnchantments.contains(other);
    }

    @Override
    public int getMinPower(int level) {
        return ObjKeys.MIN_POWER.get(this).applyAsInt(level);
        // return minpower.applyAsInt(level);
    }

    @Override
    public int getMaxPower(int level) {
        return ObjKeys.MAX_POWER.get(this).applyAsInt(level);
        // return maxpower.applyAsInt(level);
    }

    public final int getLevel(ItemStack i) {
        return EnchantmentHelper.getLevel(this, i);
    }

    public static boolean hasEnchant(Enchantment e, ItemStack i) {
        return EnchantmentHelper.getLevel(e, i) > 0;
    }

    @Override
    public final boolean isExtended() {
        return true;
    }

    // public final EnchantmentSkeleton register() {
    //     if (this.enabled) {
    //         if (this.PROJECTILE_TRACKED_DATA != null) ExtendedEnchantment.BOW_ENCHANTMENTS.add(this);
    //         final Identifier id = this.regName().contains(":") ? new Identifier(this.regname) : new Identifier("moenchantments", this.regname);
    //         if (!FabricLoader.getInstance().isModLoaded("moenchantments") && !this.regName().contains(":")) LibInit.logger.log("MODID not provided for %s. Please use the format `modid:enchantmentid` in your enchantmentBuilder", id.getPath());
    //         Registry.register(Registry.ENCHANTMENT, id, this);
    //     }
    //     return this;
    // }

    @Override
    public boolean isAcceptable(ItemStack is) {
        return ObjKeys.IS_ACCEPTIBLE.get(this).test(is);
        // return this.isAcceptible.test(is);
    }

    @Override
    public boolean isAcceptibleInAnvil(ItemStack is, AnvilScreenHandler anvil) {
        return ObjKeys.IS_ACCEPTIBLE_IN_ANVIL.get(this).apply(is, anvil);
        // return this.isAcceptibleInAnvil.apply(is, anvil);
    }

    @Override
    public boolean isAcceptableForRandomLootFunc(ItemStack stack, LootContext context) {
        return ObjKeys.IS_ACCEPTABLE_FOR_RANDOM_LOOT_FUNC.get(this).apply(stack, context);
        // return this.isAcceptableForRandomLootFunc.apply(stack, context);
    }

    @Override
    public boolean isAcceptibleOnBook() {
        return BOOLEANS[Booleans.IS_ACCEPTIBLE_ON_BOOK.ordinal()];
    }

    @Override
    public void applicationLogic(Enchantment enchantment, int power, ItemStack stack, boolean treasureAllowed,
            List<EnchantmentLevelEntry> entries) {
        ObjKeys.APPLY_LOGIC.get(this).apply(enchantment, power, stack, treasureAllowed, entries);
        // this.applyLogic.apply(enchantment, power, stack, treasureAllowed, entries);
    }

    @Override
    public AnvilAction isAcceptibleInAnvil(ItemStack left, ItemStack right, ItemStack output,
            Consumer<EnchantmentLevelEntry> consumer) {
        return ObjKeys.IS_ACCEPTIBLE_IN_ANVIL_2.get(this).check(left, right, output, consumer);
        // return this.isAcceptibleInAnvil2.check(left, right, output, consumer);
    }

    @Override
    public boolean preBlockBreak(ServerPlayerEntity player, ItemStack tool, int level, BlockPos pos) {
        return ObjKeys.PRE_BLOCK_BREAK.get(this).onBreak(player, tool, level, pos);
    }

    @Override
    public boolean postBlockBreak(ServerPlayerEntity player, ItemStack tool, int level, BlockPos pos) {
        return ObjKeys.POST_BLOCK_BREAK.get(this).onBreak(player, tool, level, pos);
    }

    @Override
    public void onCrossbowArrowCreation(PersistentProjectileEntity arrow, ItemStack crossBow, ItemStack arrowItem,
            LivingEntity shooter) {
        ObjKeys.ON_CROSSBOW_ARROW_CREATED.get(this).onCreation(arrow, crossBow, arrowItem, shooter);
    }

    @Override
    public void onBowArrowCreation(ItemStack bow, ItemStack arrowStack, PersistentProjectileEntity arrowEntity,
            PlayerEntity user, int elapsedUseTime, float pullProgress) {
        ObjKeys.ON_BOW_ARROW_CREATED.get(this).onCreation(bow, arrowStack, arrowEntity, user, elapsedUseTime, pullProgress);
    }
}