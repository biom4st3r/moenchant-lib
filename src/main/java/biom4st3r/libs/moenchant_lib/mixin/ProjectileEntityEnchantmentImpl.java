package biom4st3r.libs.moenchant_lib.mixin;

import java.util.Optional;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.World;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(PersistentProjectileEntity.class)
public abstract class ProjectileEntityEnchantmentImpl extends Entity implements EnchantableProjectileEntity {

    public ProjectileEntityEnchantmentImpl(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(at = @At(value = "RETURN"), method = "initDataTracker", cancellable = false, locals = LocalCapture.NO_CAPTURE)
    public void moenchant_lib$initDataTrackersForEnchantments(CallbackInfo ci) {
        for (TrackedData<Byte> i : ExtendedEnchantment.TRACKED_ARROW_DATA_KEYS.values()) {
            this.getDataTracker().startTracking(i, (byte)0);
        }
    }

    @Override
    public int getEnchantmentLevel(Enchantment e) {
        Optional<ExtendedEnchantment> o = ExtendedEnchantment.isExtended(e);
        if (o.isPresent()) {
            return o.get().getLevelFromProjectile((ProjectileEntity)(Object)this);
        }
        return 0;
    }

    @Override
    public int getEnchantmentLevel(ExtendedEnchantment e) {
        return e.getLevelFromProjectile((ProjectileEntity)(Object)this);
    }

    @Override
    public void setEnchantmentLevel(Enchantment e, int level) {
        Optional<ExtendedEnchantment> o = ExtendedEnchantment.isExtended(e);
        if (o.isPresent()) {
            o.get().setLevelFromProjectile((ProjectileEntity)(Object)this, level);
        }
    }

    @Override
    public void setEnchantmentLevel(ExtendedEnchantment e, int level) {
        e.setLevelFromProjectile((ProjectileEntity)(Object)this, level);
    }

    @Inject(
        at = @At(value = "TAIL"),
        method = "readCustomDataFromNbt",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    public void biom4st3r_readCustomData(NbtCompound tag, CallbackInfo ci) {
        NbtCompound ct = tag.getCompound("biom4st3r_enchantments");
        ExtendedEnchantment.TRACKED_ARROW_DATA_KEYS.keySet().forEach(k -> {
            this.getDataTracker().set(k.getProjectileEnchantmentTrackedData(), ct.getByte(k.regName()));
        });
    }

    @Inject(
        at = @At(value = "TAIL"),
        method = "writeCustomDataToNbt",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE
    )
    public void biom4st3r_writeCustomData(NbtCompound tag, CallbackInfo ci) {
        NbtCompound ct = new NbtCompound();
        ExtendedEnchantment.TRACKED_ARROW_DATA_KEYS.forEach((enchant, trackeddata) -> {
            ct.putByte(enchant.regName(), this.getDataTracker().get(trackeddata));
        });
        tag.put("biom4st3r_enchantments", ct);
    }
}