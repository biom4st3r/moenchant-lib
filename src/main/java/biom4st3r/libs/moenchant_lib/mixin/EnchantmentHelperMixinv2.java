
package biom4st3r.libs.moenchant_lib.mixin;

import java.util.Iterator;
import java.util.List;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.builder.EnchantBuilder;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixinv2 {

     @Inject(method = "getPossibleEntries", at = @At(value = "RETURN"), locals = LocalCapture.NO_CAPTURE, cancellable = true)
     private static void re_add_moenchntments(int power, ItemStack stack, boolean treasureAllowed,
               CallbackInfoReturnable<List<EnchantmentLevelEntry>> ci) {
          Iterator<ExtendedEnchantment> iter = EnchantBuilder.getEnchantments().iterator();
          List<EnchantmentLevelEntry> list = ci.getReturnValue();
          while (iter.hasNext()) {
               ExtendedEnchantment ee = iter.next();
               Enchantment e = ee.asEnchantment();
               if (ee.providesApplicationLogic()) {
                    ee.applicationLogic(ee.asEnchantment(), power, stack, treasureAllowed, list);
               } else {
                    boolean success = true;
                    boolean isBook = stack.getItem() == Items.BOOK;

                    success &= ee.isAcceptable(stack) || (isBook && ee.isAcceptibleOnBook());

                    success &= !(e.isTreasure() && !treasureAllowed);

                    success &= e.isAvailableForRandomSelection();

                    if (!success)
                         continue;
                         
                    for (int lvl = e.getMaxLevel(); lvl > e.getMinLevel() - 1; --lvl) {
                         if (power >= e.getMinPower(lvl) && power <= e.getMaxPower(lvl)) {
                              list.add(new EnchantmentLevelEntry(e, lvl));
                         }
                    }
               }
          }
     }
}
