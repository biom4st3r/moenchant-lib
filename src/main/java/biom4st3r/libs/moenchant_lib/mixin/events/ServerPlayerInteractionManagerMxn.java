package biom4st3r.libs.moenchant_lib.mixin.events;

import java.util.Map;
import java.util.Map.Entry;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.events.BlockBreakEvent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.ServerPlayerInteractionManager;
import net.minecraft.util.math.BlockPos;

@Mixin({ServerPlayerInteractionManager.class})
public abstract class ServerPlayerInteractionManagerMxn {
    @Shadow @Final
    protected ServerPlayerEntity player;

    @SuppressWarnings({"unchecked", "deprecation"})
    @Inject(at = @At(shift = Shift.BEFORE, value = "INVOKE", target = "net/minecraft/server/world/ServerWorld.removeBlock(Lnet/minecraft/util/math/BlockPos;Z)Z"), method = "tryBreakBlock")
    public void moenchant_attemptBreak(BlockPos pos, CallbackInfoReturnable<Boolean> ci) {
        Entry<Enchantment,Integer>[] i = EnchantmentHelper.get(player.getMainHandStack())
            .entrySet()
            .stream()
            .filter(entry -> ExtendedEnchantment.isExtended(entry.getKey()).isPresent())
            .toArray(Map.Entry[]::new);
        boolean failed = false;
        for (Entry<Enchantment, Integer> entry : i) {
            ExtendedEnchantment ee = (ExtendedEnchantment) entry.getKey();
            // onBreak
            if (!ee.preBlockBreak(player, player.getMainHandStack(), entry.getValue().intValue(), pos)) {
                failed = true;
                break;
            }
        }

        if (failed) {
            ci.cancel();
            return;
        }
        if (!failed) {
            for (Entry<Enchantment, Integer> entry : i) {
                ExtendedEnchantment ee = (ExtendedEnchantment) entry.getKey();
                if (!ee.postBlockBreak(player, player.getMainHandStack(), entry.getValue().intValue(), pos)) {
                    break;
                }
            }
        }
        
        if(!BlockBreakEvent.run(player.getEntityWorld(), pos, player)) ci.cancel();
    }
}
