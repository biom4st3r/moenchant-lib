package biom4st3r.libs.moenchant_lib.events;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.ScheduledForRemoval;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@AvailableSince("0.4.2")
@Deprecated(forRemoval = true)
@ScheduledForRemoval(inVersion = "0.4.5")
public final class BlockBreakEvent {

    public enum BreakStage {
        PRE_BREAK,
        POST_BREAK,
        ;
    }

    public static void register(BreakStage stage, OnBreakEvent event) {
        switch (stage) {
            case POST_BREAK:
                POST_BREAK.register(event);
                break;
            case PRE_BREAK:
                PRE_BREAK.register(event);
                break;
        }
    }

    private static Event<OnBreakEvent> PRE_BREAK = EventFactory.createArrayBacked(OnBreakEvent.class, listeners -> (world, pos, entity) -> {
        for (OnBreakEvent i : listeners) {
            if(!i.onBreak(world, pos, entity)) return false;
        }
        return true;
    });

    private static Event<OnBreakEvent> POST_BREAK = EventFactory.createArrayBacked(OnBreakEvent.class, listeners -> (world, pos, entity) -> {
        for (OnBreakEvent i : listeners) {
            if(!i.onBreak(world, pos, entity)) return false;
        }
        return true;
    });

    public static interface OnBreakEvent {
        boolean onBreak(World world, BlockPos pos, PlayerEntity entity);
    }

    public static boolean run(World world, BlockPos pos, PlayerEntity entity) {
        if(PRE_BREAK.invoker().onBreak(world, pos, entity)) {
            return POST_BREAK.invoker().onBreak(world, pos, entity);
        }
        return false;
    }
}
