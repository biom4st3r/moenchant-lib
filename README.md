# MoEnchant API
Enchantment API to enable better control of your enchantments. The main improvement is removing the usage of `EnchantmentTarget` for enchantments created with this API. `ExtendedEnchantment#isAcceptible(ItemStack)Z` is now used anytime the EnchantmentTarget would have been checked.
I also add some helpful features and events such as:
* 🔨 an Enchantment Builder class
* 🏹 automatically passing enchantments from bows -> arrows(if configured)
* 🗡️🛡️ modifiy Incoming and outgoing damage from the builder
* ♻️ customizable enchantment application logic
* 🤜 Add EntityAttributes to enchantments

## Adding to gradle
```gradle
repositories {
    maven {
        name 'MoEnchant-lib'
		url "https://gitlab.com/api/v4/projects/12070171/packages/maven/"
	}
}

dependencies {
    modImplementation "biom4st3r.mods:moenchant_lib:0.1.3"
}
```

## Basic Usage
```java
public static EnchantmentSkeleton DEATH_TOUCH = new EnchantBuilder(Rarity.VERY_RARE, {EquipmentSlot.OFFHAND, EquipmentSlot.MAINHAND})
    .minlevel(1)
    .maxlevel(1)
    .enabled(true)
    .isAcceptible(is -> is.getItem() instanceof BowItem || is.getItem() instanceof CrossbowItem)
    .addExclusive(Enchantments.POWER)
    .minpower(level->40)
    .maxpower(level->50)
    .applyEnchantmentToArrows()
    .addAttributeModifiers(EquipmentSlot.MAINHAND, multimap -> {
        multimap.put(
            EntityAttributes.GENERIC_MOVEMENT_SPEED, 
            new EntityAttributeModifier("Movement Speed", 0.25D, EntityAttributeModifier.Operation.MULTIPLY_TOTAL));
    })
    .onArrowCreated((arrow,weapon,arrowStack,shooter) -> {
        if(weapon.isDamageable()) {
            weapon.damage(Integer.MAX_VALUE, shooter.getRandom(), shooter instanceof ServerPlayerEntity spe ? spe : null);
        }
    })
    .modifyDamageOutput((lvl,target,damage,source)-> {
        target.kill();
        return TypedActionResult.FAIL(0F);
    })
    .treasure(true)
    .curse(true)
    .build("mymod:deathtoucharrow")
    .register()
```

## Json powered enchantment overrides
There is a magic file named `moenchantment_overrides.json`. This allow anyone to override some of the properties of your enchantment.
### Sample
```json
{
    "mymod:deathtoucharrow": {
        "enabled":false
    },
    "veinminer": {
        "treasure":true
    }
}
```
the modifiable fields are `minpower, maxpower, minlevel, maxlevel, enabled, treasure, curse, rarity`

`minpower and maxpower` are powered by the Exp4j library. It lets you evaluate mathematical expressions from strings with variable support. The variable used for the level is `level`.

## Contributing
Please submit all prs to Mo'Enchantments. While this is mainly an api used by my mod Mo'Enchantments atm; If your improvement breaks Mo'Enchantments don't worry about it. I'll fix it in the future. 

